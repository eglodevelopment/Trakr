//import Sidebar from "../components/sidebar"
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';
import LoadingOverlay from 'react-loading-overlay-ts';
import { useRouter } from 'next/router';

export default function Page() {
	const router = useRouter()
	const { projectId } = router.query
	const [projectName, setprojectName] = useState('')
	const [isActive, setActive] = useState(true)
	const [logs, setLogs] = useState([])
	const [url, setUrl] = useState()
	async function getLogs() {
		await axios.post('/api/logs', {
			project: projectId,
		})
			.then(function(response) {
				setUrl(response.request.responseURL)
				setLogs(response.data)
				setActive(false)
			})
			.catch(function(error) {
				location.href = '/500';
			});
	}
	async function deleteProject() {
		await axios.post('/api/projects/delete', {
			project: projectId,
		})
			.then(function(response) {
				window.location.replace("/");
			})
			.catch(function(error) {
				location.href = '/500';
			});
	}
	async function logsProject() {
		await axios.post('/api/logs/clear', {
			project: projectId,
		})
			.then(function(response) {
				getLogs()
			})
			.catch(function(error) {
				location.href = '/500';
			});
	}
	async function renameProject() {
		await axios.post('/api/projects/rename', {
			project: projectId,
			name: projectName,
		})
			.then(function(response) {
				window.location.replace("/");
			})
			.catch(function(error) {
				location.href = '/500';
			});
	}
	useEffect(() => {
		getLogs()

		const interval = setInterval(() => {
			getLogs();
		}, 5000);

		return () => clearInterval(interval);
	}, []);
	return (
		<>
			<LoadingOverlay active={isActive} fadeSpeed={400} spinner styles={{
				spinner: (base) => ({ ...base, width: '30px', '& svg circle': { stroke: 'rgba(255,255,255,255)' } }),
				overlay: (base) => ({ ...base, background: 'rgba(17,17,17,1)' }), wrapper: { width: '100%', height: '100%' }
			}}>
				<div className="text-sm breadcrumbs m-2 ml-5">
					<ul>
						<li>
							<a href="/">
								<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="w-4 h-4 mr-2 stroke-current"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M3 7v10a2 2 0 002 2h14a2 2 0 002-2V9a2 2 0 00-2-2h-6l-2-2H5a2 2 0 00-2 2z"></path></svg>
								Home
							</a>
						</li>
						<li>
							<a href={"/" + projectId}>
								<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="w-4 h-4 mr-2 stroke-current"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 13h6m-3-3v6m5 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"></path></svg>
								{projectId}
							</a>
						</li>
					</ul>
				</div>
				<div className="grid place-items-center mt-14">
					<div className="">
						<table className="table w-96">
							<thead>
								<tr>
									<th>Type</th>
									<th className="text-center">Message</th>
									<th className="text-center">Action</th>
									<th className="text-center">URL</th>
								</tr>
							</thead>
							<tbody>
								{logs.map((log, index) => (
									<tr className="hover" data-index={index}>
										<td>{log.type}</td>
										<td>{log.message}</td>
										<td>{log.action}</td>
										<td>{log.url}</td>
									</tr>
								))}
							</tbody>
						</table>
					</div>
				</div>
				<div className="fixed m-3 bottom-0 right-0 z-50">
					<div className="dropdown dropdown-top dropdown-end">
						<label tabIndex={0} className="btn m-1">Options</label>
						<ul tabIndex={0} className="dropdown-content menu p-2 shadow bg-base-100 rounded-box w-52">
							<li><label htmlFor="Clear" className="btn-clear mr-3">Clear logs</label></li>
							<li><label htmlFor="Rename" className="btn-clear mr-3">Rename project</label></li>
							<li><label htmlFor="Delete" className="btn-clear mr-3">Delete project</label></li>
							<li><label htmlFor="Connect" className="btn-clear mr-3">Connect</label></li>
						</ul>
					</div>
				</div>

				<input type="checkbox" id="Clear" className="modal-toggle" />
				<div className="modal">
					<div className="modal-box">
						<h3 className="font-bold text-lg">Clear all logs</h3>
						<p className="py-4">Are you sure you want to clear all logs this project?</p>
						<div className="modal-action">
							<label htmlFor="Clear" className="btn">Close</label>
							<label htmlFor="Clear" className="btn btn-error" onClick={logsProject}>Clear</label>
						</div>
					</div>
				</div>

				<input type="checkbox" id="Rename" className="modal-toggle" />
				<div className="modal">
					<div className="modal-box">
						<h3 className="font-bold text-lg">Rename project</h3>
						<input type="text" placeholder="Project name" className="input input-bordered input-primary w-full max-w-xs mt-5" onChange={event => setprojectName(event.target.value)} />
						<div className="modal-action">
							<label htmlFor="Rename" className="btn">Close</label>
							<label htmlFor="Rename" className="btn btn-primary" onClick={renameProject}>Rename</label>
						</div>
					</div>
				</div>

				<input type="checkbox" id="Delete" className="modal-toggle" />
				<div className="modal">
					<div className="modal-box">
						<h3 className="font-bold text-lg">Delete project</h3>
						<p className="py-4">Are you sure you want to delete this project?</p>
						<div className="modal-action">
							<label htmlFor="Delete" className="btn">Close</label>
							<label htmlFor="Delete" className="btn btn-error" onClick={deleteProject}>Delete</label>
						</div>
					</div>
				</div>

				<input type="checkbox" id="Connect" className="modal-toggle" />
				<div className="modal">
					<div className="modal-box">
						<h3 className="font-bold text-lg">Connect</h3>
						<p className="py-4">URL to send JSON to</p>
						<div className="mockup-code">
							<pre><code>{url + "/new"}</code></pre>
						</div>
						<p className="py-4">Example of JSON to send</p>
						<div className="mockup-code">
							<pre><code>{JSON.stringify({
								type: "Error",
								message: "Failed to connect to backend",
								action: "Threw a 500 page",
								url: "www.gogle.com",
								project: projectId
							}, null, 0)}</code></pre>
						</div>
						<br />
						<i className="py-4">Requests not following this format will accepted but not displayed</i>
						<div className="modal-action">
							<label htmlFor="Connect" className="btn">Close</label>
						</div>
					</div>
				</div>
			</LoadingOverlay>
		</>
	)
}
