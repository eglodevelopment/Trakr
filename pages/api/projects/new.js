// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import clientPromise from "../../../functions/mongo";

export default async function handler(req, res) {
	let id = (Math.random() + 1).toString(36).substring(2);
	const client = await clientPromise;
	const db = client.db("Trakr");
	const database_interaction = await db.collection("Projects").insertOne({name: req.body.name, id: id});
	res.status(201).json(database_interaction);
}