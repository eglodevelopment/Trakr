// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import clientPromise from "../../../functions/mongo";

export default async function handler(req, res) {
	const client = await clientPromise;
	const db = client.db("Trakr");
	const database_interaction = await db.collection("Logs").deleteMany({project: req.body.project});
	res.status(201).json(database_interaction);
}