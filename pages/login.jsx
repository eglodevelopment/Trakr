import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';

export default function Page() {
	const [userEmail, setuserEmail] = useState('')
	const [userPass, setuserPass] = useState('')
	const [alert, setAlert] = useState('')
	async function login() {
		await axios.post('/api/users/login', {
			email: userEmail,
			password: userPass
		})
			.then(function(response) {
				if (response.data.success === false) {
					setAlert(response.data.message)
				}
				if (response.data.success === true) {
					Cookies.set('token', response.data.token, {expires: 120})
					location.href = '/';
				}
			})
			.catch(function(error) {
				location.href = '/500';
			});
	}
	useEffect(() => {
	}, []);
	return (
		<>
			{(alert && alert.trim() !== '') &&
				<div className="toast toast-end">
					<div className="alert alert-error">
						<div>
							<span>{alert}</span>
						</div>
					</div>
				</div>
			}

			<div className="grid place-items-center mt-[40vh]">
				<input type="email" placeholder="Email" className="input input-bordered input-primary w-full max-w-xs" onChange={event => setuserEmail(event.target.value)} />
				<input type="password" placeholder="Password" className="input input-bordered input-primary w-full max-w-xs mt-4" onChange={event => setuserPass(event.target.value)} />
				<br />
				<button className="btn btn-wide" onClick={login}>Login</button>
			</div>
		</>
	)
}
