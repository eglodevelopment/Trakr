import Cookies from 'js-cookie'

export default async function Auth() {
	try {
		const response = await fetch(
			"/api/users/auth", {
			method: 'POST', headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				token: Cookies.get('token')
			}),
		})
		const data = await response.json();
		if (data === true) {
			console.log("Authentication successful")
		} else {
			Cookies.remove('token');
			window.location.href = "/login";
		}
	} catch {
		window.location.href = "/login";
	}
}