// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import clientPromise from "../../../functions/mongo";

export default async function handler(req, res) {
	const client = await clientPromise;
	const db = client.db("Trakr");
	try {
		const database_interaction = await db.collection("Users").find({ token: req.body.token }).toArray();
		if (database_interaction[0].token === req.body.token) {
			res.status(201).json(true);
		} else {
			res.status(201).json(false);
		}
	} catch (e) {
		res.status(201).json(false);
	}
}