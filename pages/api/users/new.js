// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import clientPromise from "../../../functions/mongo";
import validator from 'validator';

export default async function handler(req, res) {
	try {
		if (validator.isEmail(req.body.email) === false) {
			res.status(201).json({ success: false, message: "Email is not a valid email." });
			return;
		}
		if (validator.isEmpty(req.body.password) === true) {
			res.status(201).json({ success: false, message: "Password can not be empty." });
			return;
		}
		const client = await clientPromise;
		const db = client.db("Trakr");
		let token = (Math.random() + 1).toString(36).substring(2);
		const database_interaction = await db.collection("Users").insertOne({ email: req.body.email, password: req.body.password, token: token });
		res.status(201).json(database_interaction);
	} catch(e) {
		console.log(e)
		res.status(201).json({ success: false, message: "Field can not be empty." });
		return;
	}
}