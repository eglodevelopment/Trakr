// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import clientPromise from "../../../functions/mongo";
import validator from 'validator';

export default async function handler(req, res) {
	try {
		if (validator.isEmail(req.body.email) === false) {
			res.status(201).json({ success: false, message: "Email is not a valid email." });
			return;
		}
		if (validator.isEmpty(req.body.password) === true) {
			res.status(201).json({ success: false, message: "Password can not be empty." });
			return;
		}
		const client = await clientPromise;
		const db = client.db("Trakr");
		const database_interaction = await db.collection("Users").find({ email: req.body.email }).toArray();
		if (database_interaction[0].email === req.body.email) {
			if (database_interaction[0].password === req.body.password) {
				res.status(201).json({ success: true, token: database_interaction[0].token });
			} else {
				res.status(201).json({ success: false, message: "Password is incorrect." });
			}
		} else {
			res.status(201).json({ success: false, message: "Account does not exist." });
		}
	} catch (e) {
		console.log(e);
		res.status(201).json({ success: false, message: "Account does not exist." });
	}
}
