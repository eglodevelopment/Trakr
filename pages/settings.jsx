//import Sidebar from "../components/sidebar"
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';
import LoadingOverlay from 'react-loading-overlay-ts';
import { useRouter } from 'next/router';
import Auth from '../functions/auth';

export default function Page() {
	const [isActive, setActive] = useState(true)
	const [userEmail, setuserEmail] = useState('')
	const [userPass, setuserPass] = useState('')
	const [users, setUsers] = useState([])
	const [alert, setAlert] = useState('')
	async function getUsers() {
		await axios.post('/api/users', {
		})
			.then(function(response) {
				setUsers(response.data)
				setActive(false)
			})
			.catch(function(error) {
				location.href = '/500';
			});
	}
	async function deleteUser(token) {
		await axios.post('/api/users/delete', {
			token: token,
		})
			.then(function(response) {
				getUsers()
			})
			.catch(function(error) {
				location.href = '/500';
			});
	}
	async function newUser() {
		await axios.post('/api/users/new', {
			email: userEmail,
			password: userPass
		})
			.then(function(response) {
				if (response.data.success === false) {
					setAlert(response.data.message)
				}
				getUsers()
			})
			.catch(function(error) {
				location.href = '/500';
			});
	}
	useEffect(() => {
		getUsers()
		Auth()
		const interval = setInterval(() => {
			getUsers();
		}, 5000);

		return () => clearInterval(interval);
	}, []);
	return (
		<>
			<LoadingOverlay active={isActive} fadeSpeed={400} spinner styles={{
				spinner: (base) => ({ ...base, width: '30px', '& svg circle': { stroke: 'rgba(255,255,255,255)' } }),
				overlay: (base) => ({ ...base, background: 'rgba(17,17,17,1)' }), wrapper: { width: '100%', height: '100%' }
			}}>
				{(alert && alert.trim() !== '') &&
					<div className="toast toast-end z-50">
						<div className="alert alert-error">
							<div>
								<span>{alert}</span>
							</div>
						</div>
					</div>
				}
				<div className="text-sm breadcrumbs m-2 ml-5">
					<ul>
						<li>
							<a href="/">
								<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="w-4 h-4 mr-2 stroke-current"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M3 7v10a2 2 0 002 2h14a2 2 0 002-2V9a2 2 0 00-2-2h-6l-2-2H5a2 2 0 00-2 2z"></path></svg>
								Home
							</a>
						</li>
						<li>
							<a href="/settings">
								<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="w-4 h-4 mr-2 stroke-current"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M3 7v10a2 2 0 002 2h14a2 2 0 002-2V9a2 2 0 00-2-2h-6l-2-2H5a2 2 0 00-2 2z"></path></svg>
								Settings
							</a>
						</li>
					</ul>
				</div>
				<div className="grid place-items-center mt-14">
					<div className="">
						<table className="table w-96">
							<thead>
								<tr>
									<th>Email</th>
									<th className="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								{users.map((user, index) => (
									<tr className="hover" data-index={index}>
										<td>{user.email}</td>
										<td className="text-center"><button className="btn" onClick={() => deleteUser(user.token)}>Delete</button></td>
									</tr>
								))}
							</tbody>
						</table>
					</div>
				</div>
				<div className="fixed m-3 bottom-0 left-0 z-40">
					<label htmlFor="my-modal" className="btn">New user</label>

					<input type="checkbox" id="my-modal" className="modal-toggle" />
					<div className="modal">
						<div className="modal-box">
							<h3 className="font-bold text-lg">New user</h3>
							<input type="email" placeholder="User email" className="input input-bordered input-primary w-full max-w-xs mt-5" onChange={event => setuserEmail(event.target.value)} />
							<input type="password" placeholder="User password" className="input input-bordered input-primary w-full max-w-xs mt-5" onChange={event => setuserPass(event.target.value)} />
							<div className="modal-action">
								<label htmlFor="my-modal" className="btn">Close</label>
								<label className="btn btn-primary" onClick={newUser}>Add</label>
							</div>
						</div>
					</div>
				</div>
			</LoadingOverlay>
		</>
	)
}