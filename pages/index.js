//import Sidebar from "../components/sidebar"
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';
import LoadingOverlay from 'react-loading-overlay-ts';
import Auth from '../functions/auth';

export default function Page() {
	const [projectName, setprojectName] = useState('')
	const [isActive, setActive] = useState(true)
	const [projects, setProjects] = useState([])
	async function getProjects() {
		await axios.post('/api/projects', {
		})
			.then(function(response) {
				setProjects(response.data)
				setActive(false)
			})
			.catch(function(error) {
				location.href = '/500';
			});
	}
	async function newProject() {
		await axios.post('/api/projects/new', {
			name: projectName,
		})
			.then(function(response) {
				getProjects()
			})
			.catch(function(error) {
				location.href = '/500';
			});
	}
	useEffect(() => {
		getProjects()
		Auth()
	}, []);
	return (
		<>
			<LoadingOverlay active={isActive} fadeSpeed={400} spinner styles={{
				spinner: (base) => ({ ...base, width: '30px', '& svg circle': { stroke: 'rgba(255,255,255,255)' } }),
				overlay: (base) => ({ ...base, background: 'rgba(17,17,17,1)' }), wrapper: { width: '100%', height: '100%' }
			}}>
				<div className="text-sm breadcrumbs m-2 ml-5">
					<ul>
						<li>
							<a href="/">
								<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="w-4 h-4 mr-2 stroke-current"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M3 7v10a2 2 0 002 2h14a2 2 0 002-2V9a2 2 0 00-2-2h-6l-2-2H5a2 2 0 00-2 2z"></path></svg>
								Home
							</a>
						</li>
					</ul>
				</div>
				<div className="grid place-items-center mt-14">
					<div className="">
						<table className="table w-96">
							<thead>
								<tr>
									<th>Name</th>
									<th className="text-center">ID</th>
									<th className="text-center">Actions</th>
								</tr>
							</thead>
							<tbody>
								{projects.map((project, index) => (
									<tr className="hover" data-index={index}>
										<td>{project.name}</td>
										<td className="text-center">{project.id}</td>
										<td className="text-center"><a href={"/" + project.id}><button className="btn">View</button></a></td>
									</tr>
								))}
							</tbody>
						</table>
					</div>
				</div>
				<div className="fixed m-3 bottom-0 left-0 z-50">
					<a href="/settings"><button className="btn">Settings</button></a></div>
				<div className="fixed m-3 bottom-0 right-0 z-50">
					<label htmlFor="my-modal" className="btn">New project</label>

					<input type="checkbox" id="my-modal" className="modal-toggle" />
					<div className="modal">
						<div className="modal-box">
							<h3 className="font-bold text-lg">New project</h3>
							<input type="text" placeholder="Project name" className="input input-bordered input-primary w-full max-w-xs mt-5" onChange={event => setprojectName(event.target.value)} />
							<div className="modal-action">
								<label htmlFor="my-modal" className="btn">Close</label>
								<label htmlFor="my-modal" className="btn btn-primary" onClick={newProject}>Add</label>
							</div>
						</div>
					</div>
				</div>
			</LoadingOverlay>
		</>
	)
}
